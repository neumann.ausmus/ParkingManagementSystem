from django.contrib import admin
from .models import *
# Register your models here.

class CarInfoView(admin.ModelAdmin):
    list_display = ['carCode', 'carInTime', 'carOutTime', 'payDo', 'payInfo','regularCar','carLocation']

admin.site.register(CarInfo)