from django.shortcuts import render, redirect
from .models import *

# Create your views here.

#차량관리 조회
def ParkInfoMain(request):
    carinfo = CarInfo.objects.filter(setDelete=False).order_by('carInTime')
    return render(request, 'parkmanage/main.html', {'carinfo':carinfo})

#세부항목 조회 및 수정
def ParkInfoUpdate(request, pk):
    carUpdate = CarInfo.objects.get(pk=pk)
    print(carUpdate.carInTime)
    print(carUpdate.carOutTime)
    if request.method == "POST":
        print("변경")
        carUpdate.carCode = request.POST['carNumber']
        carUpdate.carInTime = request.POST['inTime']
        carUpdate.carOutTime = request.POST['outTime']
        carUpdate.payDo = request.POST['paymentStatus']
        carUpdate.payInfo = request.POST['paymentMethod']
        carUpdate.regularCar = request.POST['periodicStatus']
        carUpdate.carLocation = request.POST['location']
        carUpdate.save()
        return redirect('/')
    return render(request, 'parkmanage/update.html', {'carUpdate':carUpdate})

#삭제
def ParkInfoDelete(request, pk):
    if request.method == "POST":
        print("삭제")
        carDelete = CarInfo.objects.get(pk=pk)
        carDelete.setDelete = True
        carDelete.save()
        return redirect('/')
    return render(request, 'parkmanage/main.html')    

