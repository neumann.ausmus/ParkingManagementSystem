from django.db import models

# Create your models here.

class CarInfo(models.Model):
    class meta:
        db_table = '차량정보'
    carCode = models.CharField(max_length=20, verbose_name='차량번호')
    carInTime = models.DateTimeField(verbose_name='입차시간', null=True, blank=True)
    carOutTime = models.DateTimeField(verbose_name='출차시간', null=True, blank=True)
    payDo = models.BooleanField(default=False, verbose_name='결제유무')
    payInfo = models.CharField(max_length=20, verbose_name='결제수단')
    regularCar = models.BooleanField(default=False, verbose_name='정기차량 유무')
    carLocation = models.CharField(max_length=20, verbose_name='차량위치')
    setDelete = models.BooleanField(default=False, verbose_name='삭제여부')

    def __str__(self):
        return self.carCode