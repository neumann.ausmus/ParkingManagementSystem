from . import views
from django.urls import path

app_name = 'parkmanage'
urlpatterns= [
    path('', views.ParkInfoMain, name='ParkInfoMain'),
    path('<int:pk>/', views.ParkInfoUpdate, name='ParkInfoUpdate'),
    path('<int:pk>/delete/', views.ParkInfoDelete, name='ParkInfoDelete')
]